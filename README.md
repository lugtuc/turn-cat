# TurnCat

![image.png](./image.png)

A simple turn based messaging server to be use by turn based game clients (e.g. TEG)

## License
[GPL v3.0](LICENSE)
